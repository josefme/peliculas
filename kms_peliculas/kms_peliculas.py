# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2013 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################
from osv import osv, fields
import time

class kms_peliculas(osv.osv):
    """
    Films management
    """
    
    _name = "kms.peliculas"
    _description = "Clase que define una película"
    _columns = {
        # Title of this film
        'name': fields.char('Título de la película', size=100, required=True),
        # Director
        'director': fields.char('Director de la película', size=100, help="Es interesante, aunque no obligatorio, conocer los directores de las películas"),
        # Year of the film
        'year': fields.char('Año', size=4, required=True, help="Introduzca el año de la película"),
        # Calification
        'calification': fields.integer('Calificacion'),
        # Synopsis of the film
        'synopsis': fields.text('Sinopsis de la película'),
        # Image
        'image': fields.binary("Imagen de la película", required=True),
        # Actors
        'actors': fields.many2many( 'kms.actores', 'kms_actores_peliculas_rel', 'actor_id', 'pelicula_id', 'Actores'),
        #'actors': fields.one2many('kms.actores',  'kms.actores.name', 'Actores'),
        #'actors_ids': fields.one2many('kms.actores', 'parent_id', 'Name', required=False, help="Help"),
        # Award
        #'award': fields.one2many('kms.premios.peliculas', 'award_id', 'Premios'),
    }
    
    _defaults = {
        # Year by default:
        'year': lambda *a:  str(time.strftime('%Y')),
    }
    

kms_peliculas()


class kms_premios_peliculas(osv.osv):
    """
    Premios recibidos por las películas
    """ 
    _name = "kms.premios.peliculas"
    _description = "Clase que define a un premio de una película"
    _columns = {
        # Name of this award
        'name': fields.char('Nombre del premio', size=100, required=True),
        # Date of the award
        'date': fields.date('Fecha de entrega del premio', help="Introduzca la fecha en la que se entregó el premio", required=True),
        # Notes of the actor
        'notes': fields.text('Notas'),
        # Image
        'image': fields.binary("Foto del premio", required=True),
        # Film winner
        'award_id': fields.many2one('kms.peliculas', 'Pelicula que ha ganado el premio', select=True), 
    }
    
    _defaults = {
        # Sex by default:
        'date': lambda *a: time.strftime('%Y-%m-%d'),
    }
kms_premios_peliculas()

class kms_actores(osv.osv):
    """
    Actors of the films
    """
    
    _name = "kms.actores"
    _description = "Clase que define a un actor de cine"
    _columns = {
        # Title of this film
        'name': fields.char('Nombre del actor', size=100, required=True),
        # Date of the birth
        'date': fields.date('Fecha de nacimiento', help="Introduzca la fecha de nacimiento del actor"),
        # Sex of the actor
        'sex': fields.selection([('V','Hombre'),('M','Mujer')],'Sexo', required=True),
        # Notes of the actor
        'notes': fields.text('Notas'),
        # Image
        'image': fields.binary("Fotografía", required=True),
        # Films
        'films': fields.many2many( 'kms.peliculas', 'kms_peliculas_actores_rel', 'pelicula_id', 'actor_id', 'Peliculas'),
    }
    
    _defaults = {
        # Sex by default:
        'sex': "M",
    }
    
kms_actores()


class kms_peliculas(osv.osv):
    _inherit = "kms.peliculas"
    _columns = {
           'actors': fields.many2many( 'kms.actores', 'kms_peliculas_actores_rel', 'actor_id', 'pelicula_id', 'Actores'), 
           #'actors': fields.one2many('kms.actores', 'kms.actores.name', 'Actores'),
           'award': fields.one2many('kms.premios.peliculas', 'award_id', 'Premios'),
    }

kms_peliculas()

